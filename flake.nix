{
  description = "Creerio's nixos config";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    nixpkgsBuild.url = "github:nixos/nixpkgs/release-24.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-master.url = "github:nixos/nixpkgs/master";
    # Nix hardware, contains some configurations
    hardware.url = "github:nixos/nixos-hardware";

    # Home manager for home config & user packages
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # For plasma config
    plasma-manager = {
      url = "github:pjones/plasma-manager";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };

    # packwiz.url = "github:packwiz/packwiz"; # Up to date in nixos unstable
  };

  outputs =
    {
      nixpkgs,
      nixpkgsBuild,
      nixpkgs-unstable,
      nixpkgs-master,
      hardware,
      home-manager,
      plasma-manager,
      ...
    }:
    let
      version = "24.11";
      system = "x86_64-linux";
      username = "creerio";
      hostname = "nuxos";
      unstable = import nixpkgs-unstable {
        inherit system;
        config.allowUnfree = true;
      };
      master = import nixpkgs-master {
        inherit system;
        config.allowUnfree = true;
      };
      pkg-overlay = final: prev: {
        build = import nixpkgsBuild {
          inherit system;
          config.allowUnfree = true;
        };
        unstable = unstable;
      };
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
        overlays = [ pkg-overlay ];
      };
      # options = import ./options.nix;

      # Libs for MC dev devShells
      mcLibs = with pkgs.unstable; [
        libpulseaudio
        libGL
        glfw
        openal
        stdenv.cc.cc.lib
      ];

    in
    # Wrong packwiz version in repos, using 'fixed' updated version from their github
    # packwizPkg = packwiz.packages."${system}".default;
    {
      nixosConfigurations = {
        "${hostname}" = nixpkgs.lib.nixosSystem {

          # Allow use of stable & unstable packages
          inherit pkgs;
          specialArgs = {
            inherit hostname username nixpkgs-unstable;
          };

          modules = [
            {
              # disabledModules = [
              #   "services/x11/xserver.nix"
              #   "services/desktop-managers/plasma6.nix"
              # ];
              # imports = [
              #   "${nixpkgs-unstable}/nixos/modules/services/x11/xserver.nix"
              #   "${nixpkgs-unstable}/pkgs/development/libraries/gdk-pixbuf/default.nix"
              #   "${nixpkgs-unstable}/nixos/modules/services/desktop-managers/plasma6.nix"
              # ];
              # nixpkgs.overlays = [
              #   (self: super: {
              #     kdePackages = nixpkgs-unstable.legacyPackages.x86_64-linux.kdePackages;
              #   })
              # ];

              # This value determines the NixOS release from which the default
              # settings for stateful data, like file locations and database versions
              # on your system were taken. It‘s perfectly fine and recommended to leave
              # this value at the release version of the first install of this system.
              # Before changing this value read the documentation for this option
              # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
              system.stateVersion = "${version}"; # Did you read the comment?
            }
            ./root/configuration.nix

            # Nix hardware config, see https://github.com/NixOS/nixos-hardware
            hardware.nixosModules.common-gpu-amd
            hardware.nixosModules.common-cpu-amd
            hardware.nixosModules.common-pc-ssd

            # Home manager config
            home-manager.nixosModules.home-manager
            {
              home-manager.useUserPackages = true;
              home-manager.extraSpecialArgs = {
                inherit unstable master;
              };
              home-manager.backupFileExtension = "backup";

              home-manager.users."${username}" = {
                # Unfortunately not possible..?
                # nixpkgs = pkgs;

                # Config
                imports = [
                  {
                    nixpkgs.config.allowUnfree = true;
                    home.username = "${username}";
                    home.homeDirectory = "/home/${username}";
                    home.stateVersion = "${version}";
                    programs.home-manager.enable = true;
                  }
                  plasma-manager.homeManagerModules.plasma-manager

                  ./home/home.nix
                ];
              };
            }
          ];
        };
      };
      devShells = {
        mc17 = pkgs.mkShell {
          name = "mcDev";
          buildInputs = with pkgs.unstable; [ jdk17 ] ++ mcLibs;
          LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath mcLibs;
          shellHook = ''
            idea-ultimate&
            exit
          '';
        };
        mc21 = pkgs.mkShell {
          name = "mcDev";
          buildInputs = with pkgs.unstable; [ jdk21 ] ++ mcLibs;
          LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath mcLibs;
          shellHook = ''
            idea-ultimate&
            exit
          '';
        };
      };

      #homeConfigurations = {
      #  "${username}" = home-manager.lib.homeManagerConfiguration {

      # Allow use of stable & unstable packages
      #    inherit pkgs;

      #    modules = [
      #      {
      #        home.username = "${username}";
      #        home.homeDirectory = "/home/${username}";
      #        home.stateVersion = "23.11";
      #        programs.home-manager.enable = true;
      #      }
      #      plasma-manager.homeManagerModules.plasma-manager
      #      ./home/home.nix
      #    ];
      #  };
      #};
    };
}
