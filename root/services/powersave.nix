{ ... }:
{
  systemd.services.powersave = {
    enable = true;
    description = "Modifies some values to reduce power consumption, in this case : Enable audio codec management, autosuspend G502 & Mystic light";
    wantedBy = [ "multi-user.target" ];
    script = ''
      echo '1' > '/sys/module/snd_hda_intel/parameters/power_save' && echo 'auto' > '/sys/bus/usb/devices/1-6.1/power/control' && echo 'auto' > '/sys/bus/usb/devices/1-8/power/control'
    '';
    unitConfig = {
      Type = "oneshot";
    };
  };
}
