{ pkgs, ... }:
{
  services.pipewire.extraConfig.pipewire = {
    "20-games_out" = {
      "context.objects" = [
        {
          "factory" = "adapter";
          "args" = {
            "factory.name" = "support.null-audio-sink";
            "node.name" = "Games_OUTput";
            "node.description" = "Games_OUTput";
            "media.class" = "Audio/Sink";
            "audio.position" = "FL, FR";
            "monitor.channel-volumes" = true;
          };
        }
      ];
    };
    "20-music_obs_out" = {
      "context.objects" = [
        {
          "factory" = "adapter";
          "args" = {
            "factory.name" = "support.null-audio-sink";
            "node.name" = "OBS_Music_OUTput";
            "node.description" = "OBS_Music_OUTput";
            "media.class" = "Audio/Sink";
            "audio.position" = "FL, FR";
            "monitor.channel-volumes" = true;
          };
        }
      ];
    };
    "20-music_out" = {
      "context.objects" = [
        {
          "factory" = "adapter";
          "args" = {
            "factory.name" = "support.null-audio-sink";
            "node.name" = "Music_OUTput";
            "node.description" = "Music_OUTput";
            "media.class" = "Audio/Sink";
            "audio.position" = "FL, FR";
            "monitor.channel-volumes" = true;
          };
        }
      ];
    };
    "20-other_out" = {
      "context.objects" = [
        {
          "factory" = "adapter";
          "args" = {
            "factory.name" = "support.null-audio-sink";
            "node.name" = "Other_OUTput";
            "node.description" = "Other_OUTput";
            "media.class" = "Audio/Sink";
            "audio.position" = "FL, FR";
            "monitor.channel-volumes" = true;
          };
        }
      ];
    };
    "20-vocal_out" = {
      "context.objects" = [
        {
          "factory" = "adapter";
          "args" = {
            "factory.name" = "support.null-audio-sink";
            "node.name" = "Vocal_OUTput";
            "node.description" = "Vocal_OUTput";
            "media.class" = "Audio/Sink";
            "audio.position" = "FL, FR";
            "monitor.channel-volumes" = true;
          };
        }
      ];
    };
    "99-rnnoise" = {
      "context.modules" = [
        {
          "name" = "libpipewire-module-filter-chain";
          "args" = {
            "node.description" = "Noise Canceling source";
            "media.name" = "Noise Canceling source";
            "filter.graph" = {
              "nodes" = [
                {
                  "type" = "ladspa";
                  "name" = "rnnoise";
                  "plugin" = "${pkgs.rnnoise-plugin}/lib/ladspa/librnnoise_ladspa.so";
                  "label" = "noise_suppressor_mono";
                  "control" = {
                    "VAD Threshold (%)" = 50.0;
                    "VAD Grace Period (ms)" = 200;
                    "Retroactive VAD Grace (ms)" = 0;
                  };
                }
              ];
            };
            "audio.position" = [ "MONO" ];
            "capture.props" = {
              "node.name" = "capture.rnnoise_source";
              "node.passive" = true;
              "audio.rate" = 48000;
            };
            "playback.props" = {
              "node.name" = "rnnoise_source";
              "media.class" = "Audio/Source";
              "audio.rate" = 48000;
            };
          };
        }
      ];
    };
  };
}
