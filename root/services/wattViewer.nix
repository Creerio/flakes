{ ... }:
{
  systemd.services.watt_viewer = {
    enable = true;
    description = "Modifies the permission of a file for the CPU Power monitor kde extension";
    wantedBy = [ "multi-user.target" ];
    unitConfig = {
      Type = "simple";
    };
    serviceConfig = {
      ExecStart = "/run/current-system/sw/bin/chmod 444 /sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/energy_uj";
    };
  };
}
