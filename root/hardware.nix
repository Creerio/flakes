# Hardware related
{
  config,
  pkgs,
  ...
}:
{
  # Kernel
  boot.kernelPackages = with pkgs; unstable.linuxPackages_zen;
  boot.extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback ];
  boot.kernelModules = [
    "v4l2loopback"
  ];

  # Graphics drivers
  services.xserver.videoDrivers = [ "amdgpu" ];
  services.xserver.enableTearFree = true;
  services.xserver.deviceSection = ''
    Option "VariableRefresh" "true"
  '';
  # Using dual screen setup
  services.xserver.displayManager.setupCommands = ''
    ${pkgs.xorg.xrandr}/bin/xrandr --output DisplayPort-0 --mode 1920x1080 -r 143.85 --pos 1920x0 --rotate normal --output DisplayPort-1 --mode 1920x1080 -r 143.85 --pos 0x0 --rotate normal
  '';

  # Graphics + 32 bits
  hardware.graphics = {
    enable = true;
    enable32Bit = true;
    # extraPackages = [
    # pkgs.rocm-opencl-icd
    # pkgs.rocm-opencl-runtime
    # pkgs.rocmPackages.rccl
    # ];
  };

  # Enable CUPS to print documents
  hardware.sane.enable = true;
  hardware.sane.extraBackends = [ pkgs.hplipWithPlugin ];
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplipWithPlugin ];

  # Nerf power management to NOT eat power when it shouldn't
  powerManagement = {
    enable = true;
    #cpuFreqGovernor = "ondemand"; # Disabled, 5700G uses amd_pstate=active by default on performance mode
  };
}
