# Nix config
{ ... }:
{
  nix = {
    settings = {
      # Enable flakes + 'nix' cmd
      experimental-features = "nix-command flakes";

      auto-optimise-store = true;
    };
    gc = {
      automatic = true;
      dates = "weekly";
    };
  };
}
