{ pkgs, ... }:
{
  users.users.creerio = {
    isNormalUser = true;
    description = "creerio";
    extraGroups = [
      "networkmanager"
      "wheel"
      "vboxusers"
      "libvirtd"
      "corectrl"
      "scanner"
      "lp"
      "rtkit"
      "adbusers"
    ]; # Network, sudo, virtbox, qemu, corectrl launched without password, scanner/printing, same, realtime for audio & other
    packages = with pkgs; [
      jdk8
      jdk11
      jdk17
      jdk21
      dconf # Used for theme
      partition-manager
      # soundux # No longer maintained, flatpak instead
    ];
  };

}
