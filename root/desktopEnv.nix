{ pkgs, ... }:
{
  # Use X11
  services.xserver.enable = true;

  # Display manager
  services.displayManager.sddm.enable = true;
  services.displayManager.sddm.autoNumlock = true;
  services.displayManager.defaultSession = "plasmax11";

  # Desktop env
  services.desktopManager.plasma6.enable = true;

  # Sound server
  # sound.enable = true;
  hardware.pulseaudio.enable = false;
  # Enable realtime for pipewire
  security.rtkit.enable = true;
  services.pipewire = {
    # Full pipewire w/ wireplumber
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  xdg = {
    portal = {
      enable = true;
      extraPortals = with pkgs; [
        #xdg-desktop-portal-kde
      ];
    };
  };
}
