# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ ... }:

{
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix

    # System settings
    ./hardware.nix
    ./bootloader.nix
    ./network.nix
    ./language.nix
    ./desktopEnv.nix
    ./nix.nix
    ./packages.nix
    ./users.nix
    ./services.nix
    # ./vmSettings.nix
  ];
}
