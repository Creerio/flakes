# Services/Packages
{ pkgs, ... }:
{
  imports = [
    ./services/pipewire.nix
    ./services/powersave.nix
    ./services/wattViewer.nix
  ];

  # Enable kde wallet for apps
  security.pam.services.kwallet = {
    name = "kwallet";
    enableKwallet = true;
  };

  # Virtual box
  virtualisation.virtualbox.host.enable = true;

  # QEMU/KVM
  virtualisation.libvirtd.enable = true;
  virtualisation.libvirtd.qemu.package = pkgs.qemu_full;
  virtualisation.libvirtd.qemu.swtpm.enable = true;
  virtualisation.libvirtd.qemu.ovmf.enable = true;
  virtualisation.libvirtd.qemu.ovmf.packages = with pkgs; [
    OVMFFull.fd
    pkgsCross.aarch64-multiplatform.OVMF.fd
  ];
  virtualisation.spiceUSBRedirection.enable = true;
  services.spice-vdagentd.enable = true;

  # Flatpak => Remote add must be executed manually !
  services.flatpak.enable = true;
  # FontDir for flatpak & other
  fonts.fontDir.enable = true;

  # Piper support
  services.ratbagd.enable = true;

  # FWUPD
  services.fwupd.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Printer on network
  services.avahi.enable = true;
  services.avahi.nssmdns4 = true;
  services.avahi.nssmdns6 = true;
  services.avahi.openFirewall = true;

}
