{ ... }:
{
  programs.steam = {
    enable = true;
    #package = unstable.steam;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
  };
}
