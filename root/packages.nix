# Packages in general
{ pkgs, ... }:
{
  imports = [
    ./packages/corectrl.nix
    ./packages/fonts.nix
    ./packages/gnupg.nix
    ./packages/steam.nix
  ];

  # Man pages added by libs/dev tools
  documentation.dev.enable = true;

  # Remove unneeded KDE apps
  environment.plasma6.excludePackages = with pkgs.unstable; [
    kdePackages.elisa
    kdePackages.khelpcenter
    kdePackages.discover
    kdePackages.krdp
    ibus
  ];

  # Remove unneeded apps
  services.xserver.excludePackages = with pkgs; [
    xterm
  ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    lm_sensors
    kdePackages.libksysguard
    mesa
    kdePackages.sddm-kcm
    virtiofsd
    rocmPackages.rocminfo
    unrar # Rar files not supported by default
    man-pages # Man pages are not complete
    man-pages-posix
    libguestfs-with-appliance
    kdePackages.qtwebsockets
  ];

  programs.kdeconnect.enable = true;

  programs.adb.enable = true;

  # KDE partition manager (for super user support)
  programs.partition-manager.enable = true;
}
