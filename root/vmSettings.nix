# VM settings
{ ... }:
{
  users.users.creerio.initialHashedPassword = "$y$j9T$V7VW9ZVFcR5d/WDh/QlOD/$D..3EH6aDlobUgaz4mcYlQjiiQfUlCk1.LLU4Ak4tY2";
  users.users.test.initialHashedPassword = "$y$j9T$V7VW9ZVFcR5d/WDh/QlOD/$D..3EH6aDlobUgaz4mcYlQjiiQfUlCk1.LLU4Ak4tY2";
  users.users.test.isNormalUser = true;
  users.users.test.group = "test";
  users.groups.test = { };
  virtualisation.vmVariant = {
    # following configuration is added only when building VM with build-vm
    virtualisation = {
      memorySize = 4048; # Use 2048MiB memory.
      cores = 4;
    };
  };
}
