# Bootloader stuff
{ ... }:
{
  boot.loader = {
    # EFI variables for grub
    efi = {
      canTouchEfiVariables = true;
      #efiSysMountPoint = "/boot/efi";
    };
    # Enable grub for efi (nodev + efiSupport) only & enable os prober for dual-boot
    grub = {
      enable = true;
      devices = [ "nodev" ];
      efiSupport = true;
      useOSProber = true;
      configurationLimit = 20;
      extraEntries = ''
        menuentry "Reboot" {
            reboot
        }
        menuentry "Poweroff" {
            halt
        }
      '';
    };
  };
}
