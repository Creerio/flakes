{ pkgs, ... }:
{
  gtk = {
    enable = true;
    cursorTheme = {
      # package = ;
      name = "Breeze_Light";
      size = 24;
    };
    font = {
      name = "Noto Sans";
      size = 10;
    };
    iconTheme = {
      package = pkgs.kdePackages.breeze-gtk;
      name = "breeze-dark";
    };
    theme = {
      package = pkgs.kdePackages.breeze-gtk;
      name = "Breeze";
    };
  };

  qt = {
    enable = true;
    style = {
      name = "breeze";
      package = pkgs.kdePackages.breeze;
    };
  };
}
