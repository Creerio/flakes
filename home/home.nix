{ ... }:

{
  # Reload services when changing configs
  systemd.user.startServices = "sd-switch";

  imports = [
    ./config.nix
    ./packages.nix
    ./services.nix
    ./theme.nix
  ];

  # You can also manage environment variables but you will have to manually
  # source
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/test-mdp-test/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.
  systemd.user.sessionVariables = {
    EDITOR = "nano";
    GTK_THEME = "Breeze";
  };
}
