{ pkgs, ... }:
let
  glab = {
    user = {
      name = "Creerio";
      email = "10045021-Creerio@users.noreply.gitlab.com";
      signingkey = "7D4139D8C68B0D00"; # TODO : SAVE GPG
    };
  };
  ghub = {
    user = {
      name = "Creerio";
      email = "44642117+Creerio@users.noreply.github.com";
      signingkey = "8D56FF4ADB5717F4";
    };
  };
in
{
  programs.git = {
    enable = true;
    package = pkgs.gitFull;
    extraConfig = {
      color = {
        ui = true;
      };
      core = {
        editor = "nano";
      };
      commit = {
        gpgsign = true;
      };
    };

    includes = [
      {
        condition = "hasconfig:remote.*.url:git@gitlab.com:**/**";
        contents = glab;
      }
      {
        condition = "hasconfig:remote.*.url:https://gitlab.com/**/**";
        contents = glab;
      }
      {
        condition = "hasconfig:remote.*.url:git@github.com:**/**";
        contents = ghub;
      }
      {
        condition = "hasconfig:remote.*.url:https://github.com/**/**";
        contents = ghub;
      }
    ];
  };
}
