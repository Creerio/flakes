{ pkgs, ... }:
{
  programs.zsh = {
    enable = true;
    enableCompletion = true;
    syntaxHighlighting.enable = true;
    autosuggestion.enable = true;
    historySubstringSearch.enable = true;
    history = {
      ignoreAllDups = true;
      ignoreSpace = true;
    };
    plugins = [
      {
        name = "zsh-nix-shell";
        file = "share/zsh-nix-shell/nix-shell.plugin.zsh";
        src = pkgs.zsh-nix-shell;
      }
      {
        name = "powerlevel10k";
        file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
        src = pkgs.zsh-powerlevel10k;
      }
      {
        name = "zsh-fzf-tab";
        file = "share/fzf-tab/fzf-tab.plugin.zsh";
        src = pkgs.zsh-fzf-tab;
      }
    ];
    initExtra = ''
      # No need to autoload, having zsh-autosuggestions already predefines that
      # autoload -U compinit && compinit
      if [ -n "''\'''${commands[fzf-share]}'" ]; then
        source "$(fzf-share)/key-bindings.zsh"
        source "$(fzf-share)/completion.zsh"
      fi

      source ~/.p10k.zsh

      # Navigate words
      bindkey '^[Oc' forward-word                                     #
      bindkey '^[Od' backward-word                                    #
      bindkey '^[[1;5D' backward-word                                 #
      bindkey '^[[1;5C' forward-word                                  #
      # Keybinds
      bindkey '^H' backward-kill-word                                 # delete previous word with ctrl+backspace
      bindkey '5~' kill-word                                          # delete next word using ctrl+supr
      bindkey '^[[Z' undo                                             # Shift+tab undo last action
      bindkey "^[[3~" delete-char
      bindkey '^[[5~' history-beginning-search-backward               # Page up key
      bindkey '^[[6~' history-beginning-search-forward                # Page down key

      # Completion styling
      zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
      zstyle ':completion:*' menu no
      zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'

      # Colored Man
      export MANPAGER="less -M -R -i --use-color -Dd+G -Du+B -DHkC -j5"
      export GROFF_NO_SGR=1         # Konsole / Gnome-terminal ignore colors
    '';
    shellAliases = {
      flake-update = "cd ~/nixos && nix flake update";
      garbage-collect = "home-manager expire-generations '-7 days' && sudo nix-env --delete-generations old && sudo nix-collect-garbage -d";
      gadd = "git add";
      gaddall = "git add . && git status";
      gamend = "git commit --amend";
      gcommit = "git commit -S";
      gclone = "git clone";
      gfetch = "git fetch";
      gforcepush = "git push -f";
      gforcereset = "git reset --hard";
      ginteractiverebase = "git rebase -i";
      gpull = "git pull";
      gpush = "git push";
      grebase = "git rebase";
      greset = "git reset";
      gstatus = "git status";
      nix-search = "nix-env -qa | fzf";
    };
  };
}
