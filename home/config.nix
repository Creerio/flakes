# Config files

{ ... }:
{
  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
    #".config/QtProject/QtCreator.ini" = ''
    #  [Core]
    #  CreatorTheme=dark
    #'';
  };

  dconf.settings = {
    "org/virt-manager/virt-manager/connections" = {
      autoconnect = [ "qemu:///system" ];
      uris = [ "qemu:///system" ];
    };
  };

  # Kde plasma config w/ plasma-manager
  programs.plasma.configFile = {
    # "baloofilerc"."Basic Settings"."Indexing-Enabled" = false;
    # "kcminputrc"."Keyboard"."NumLock" = 0;
    # "kcminputrc"."Mouse"."cursorTheme" = "Breeze_Light";
    # "kdeglobals"."General"."BrowserApplication" = "firefox.desktop";
    # "kdeglobals"."KDE"."SingleClick" = false;
    # "krunnerrc"."Plugins"."baloosearchEnabled" = false;
    # "ksmserverrc"."General"."loginMode" = "emptySession";
    # "ksplashrc"."KSplash"."Engine" = "none";
    # "kwalletrc"."Wallet"."Enabled" = true;
    # "kwinrc"."Plugins"."blurEnabled" = true;
    # "kwinrc"."Plugins"."contrastEnabled" = true;
    # "kwinrc"."TabBox"."LayoutName" = "thumbnail_grid";
    # "plasmanotifyrc"."Notifications"."PopupPosition" = "TopRight";
    # "plasmanotifyrc"."Notifications"."PopupTimeout" = 5000;
  };
}
