# Adds base packages
{
  pkgs,
  unstable,
  master,
  ...
}:
{
  imports = [
    ./packages/git.nix
    ./packages/zsh.nix
  ];

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [
    # # Adds the 'hello' command to your environment. It prints a friendly
    # # "Hello, world!" when run.
    # pkgs.hello

    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')

    # Office
    kdePackages.kate
    kdePackages.okular
    libreoffice-still
    # ganttproject-bin
    # texliveFull
    # texstudio

    # Dev
    qtcreator
    # qt6.qttools
    cmake
    gcc
    ninja
    gdb
    valgrind
    gnumake
    #jdk8
    #jdk11 # Multiple versions = conflict, added on root config to user
    #jdk17
    #jdk21
    sqlitebrowser
    # php
    # php81Packages.composer
    # emacs
    # opam
    shellcheck
    nodejs_20
    npm-check-updates
    gradle
    maven
    unstable.jetbrains.idea-ultimate
    unstable.jetbrains.webstorm
    # unstable.jetbrains.clion
    unstable.jetbrains.datagrip
    # androidStudioPackages.stable
    # python311Full
    #(python311Full.withPackages(ps: with ps; [ pip setuptools ])) # scikit-learn pandas xlrd matplotlib numpy
    unstable.zed-editor

    # Unused
    # unstable.jetbrains.phpstorm
    # unstable.jetbrains.pycharm-professional

    # Graphism
    gimp
    inkscape
    krita
    kdePackages.spectacle
    kdePackages.gwenview
    kdePackages.skanlite
    peek
    # blockbench-electron
    kdePackages.kcolorchooser
    kdePackages.kolourpaint

    # Internet
    mumble
    firefox
    librewolf
    brave
    tor-browser-bundle-bin
    kdePackages.falkon
    midori
    chromium
    filezilla
    discord
    # (unstable.discord-canary.override { withOpenASAR = true; })
    # ((unstable.discord-canary.override { vencord = unstable.vencord; }).override { withOpenASAR = true; withVencord = true; })
    # ((unstable.discord-canary.override { vencord = master.vencord; }).override {
    #   withOpenASAR = true;
    #   withVencord = true;
    # })
    ((unstable.discord-canary.override { vencord = (callPackage ./packages/vencord.nix { }); }).override
      {
        withOpenASAR = true;
        withVencord = true;
      }
    )
    unstable.revolt-desktop
    signal-desktop
    remmina
    mullvad-browser

    # Games
    unstable.lutris
    # unstable.minecraft # Broken.. oh no ! Anyways.
    unstable.srb2kart
    unstable.prismlauncher
    unstable.melonDS
    unstable.bottles
    superTuxKart

    # Multimedia
    vlc
    mpv
    clementine
    handbrake
    kdePackages.kdenlive
    picard
    unstable.obs-studio
    qpwgraph
    easyeffects
    kdePackages.plasmatube
    # unstable.coppwr

    # System
    # partition-manager # enabled by programs.partition-manager.enable = true; on root config
    kdePackages.konsole
    kdePackages.plasma-systemmonitor
    # mission-center
    #libsForQt5.ksysguard # No longer available
    bleachbit
    # timeshift
    gnome-disk-utility
    kdePackages.ksystemlog
    virt-manager
    virt-viewer
    # qemu_full
    # virtualbox # Enabled on root config

    # Utils
    keepassxc
    kdePackages.kleopatra
    kdePackages.kcalc
    nextcloud-client
    ventoy-full
    # hardinfo
    protontricks
    protonup-qt
    metadata-cleaner
    # corectrl # Enabled on root config
    piper
    warp
    impression
    filelight
    # wineWowPackages.stable # Support 32 & 64 bits win apps
    # winetricks
    # bubblewrap
    fastfetch
    unstable.universal-android-debloater
    nixd
    nil
    nixfmt-rfc-style

    # CMD
    yt-dlp
    whois
    nmap
    curl
    wget
    scrcpy
    # packwizPkg # Packwiz in distro repo was bugged, used updated 'working' version with flake from their repo
    unstable.packwiz
    htop
    killall
    powertop
    nvtopPackages.amd
    usbutils
    home-manager
    xorg.xkill
    fzf # ZSH

    # Other
    kdePackages.kdeconnect-kde
    aspell
    aspellDicts.fr
    aspellDicts.en
    aspellDicts.en-computers
    aspellDicts.en-science
    rnnoise-plugin
  ];

  nixpkgs.config.permittedInsecurePackages = [
    "electron-25.9.0" # blockbench
  ];
}
