# Package induced services
{ ... }:
{
  #services.nextcloud-client.enable = true;

  # Enable only if flake update is needed
  #   systemd.user.services.flake_update = {
  #     Unit= {
  #       Description = "Launches at startup the nix flake update command, keeping the system flake up to date";
  #     };
  #     Install = {
  #       WantedBy = [ "default.target" ];
  #     };
  #     Service = {
  #        ExecStart = "/run/current-system/sw/bin/nix flake update /home/creerio/nixos/ -vv";
  #     };
  #   };
  # systemd.user.services.pipewire_connector = {
  #   Unit = {
  #     Description = "Connects pipewire outputs to the user's default output";
  #   };
  #   Install = {
  #     WantedBy = [ "default.socket" ];
  #   };
  #   Service = {
  #     ExecStart = "/run/current-system/sw/bin/pw-link Games_OUTput:monitor_FL alsa_output.usb-Generic_Turtle_Beach_P11_Headset_0000000001-00.analog-stereo:playback_FL; /run/current-system/sw/bin/pw-link Games_OUTput:monitor_FR alsa_output.usb-Generic_Turtle_Beach_P11_Headset_0000000001-00.analog-stereo:playback_FR; /run/current-system/sw/bin/pw-link OBS_Music_OUTput:monitor_FL alsa_output.usb-Generic_Turtle_Beach_P11_Headset_0000000001-00.analog-stereo:playback_FL; /run/current-system/sw/bin/pw-link OBS_Music_OUTput:monitor_FR alsa_output.usb-Generic_Turtle_Beach_P11_Headset_0000000001-00.analog-stereo:playback_FR; /run/current-system/sw/bin/pw-link Music_OUTput:monitor_FL alsa_output.usb-Generic_Turtle_Beach_P11_Headset_0000000001-00.analog-stereo:playback_FL; /run/current-system/sw/bin/pw-link Music_OUTput:monitor_FR alsa_output.usb-Generic_Turtle_Beach_P11_Headset_0000000001-00.analog-stereo:playback_FR; /run/current-system/sw/bin/pw-link Other_OUTput:monitor_FL alsa_output.usb-Generic_Turtle_Beach_P11_Headset_0000000001-00.analog-stereo:playback_FL; /run/current-system/sw/bin/pw-link Other_OUTput:monitor_FR alsa_output.usb-Generic_Turtle_Beach_P11_Headset_0000000001-00.analog-stereo:playback_FR; /run/current-system/sw/bin/pw-link Vocal_OUTput:monitor_FL alsa_output.usb-Generic_Turtle_Beach_P11_Headset_0000000001-00.analog-stereo:playback_FL; /run/current-system/sw/bin/pw-link Vocal_OUTput:monitor_FR alsa_output.usb-Generic_Turtle_Beach_P11_Headset_0000000001-00.analog-stereo:playback_FR";
  #   };
  # };
}
