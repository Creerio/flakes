# My nixos config

This repo contains my personal nixos configuration. If you are an experienced nix user, you may have realized that this config is rather.. 'user friendly'. This is normal as I am new to nixos (and obviously everything is not as easy as I thought).

## Folders

- root : Root config, specific to my configuration. (if you take it, please modify some elements to adapt them to your system specs).
- home : Home-manager configuration. Contains a flake file, packages, a theme config, a zsh config and some plasma settings.

## How to use

1. Modify the root config to adapt it to your system specs. The [flake](flake.nix) file may also be modified for nixhardware modules
2. Run nixos-rebuild the way you want to. For example : `nixos-rebuild switch --upgrade`
